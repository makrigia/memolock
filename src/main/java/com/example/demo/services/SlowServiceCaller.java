package com.example.demo.services;

import com.example.demo.MemoLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SlowServiceCaller {
    @Autowired
    private SlowService slowService;

    @MemoLock
    public String call() {
        return slowService.fetch();
    }

}
