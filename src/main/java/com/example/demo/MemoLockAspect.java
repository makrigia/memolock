package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import static java.util.concurrent.TimeUnit.SECONDS;

@Aspect
@Slf4j
@Component
public class MemoLockAspect {
    @Autowired
    private RedisMessageListenerContainer container;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Around("@annotation(MemoLock)")
    public Object memoLockImplementation(ProceedingJoinPoint joinPoint) throws Throwable {
        //a reference for the result that will be returned
        final Object[] result = new Object[1];

        //used for calculating the cache result/lock/channel key
        Object args = SimpleKeyGenerator.generateKey(joinPoint.getArgs());
        String methodName = joinPoint.getSignature().toString();

        String resultKey = "result:" + methodName + "_" + args.toString();

        Object valueFromRedis = getValueFromRedis(resultKey);
        // if value already exists in cache, then we're done
        if (valueFromRedis != null) {
            return "redis: " + valueFromRedis;
        }

        String lockKey = "lock:" + methodName + "_" + args.toString();
        String notificationKey = "notify:" + methodName + "_" + args.toString();

        //try to set the lockKey if it's not there.
        // If it's there, it means that someone is currently calculating it
        Boolean managedToSet = redisTemplate.opsForValue().setIfAbsent(lockKey, "", 10, SECONDS);
        PseudoLock pseudoLock = new PseudoLock();
        pseudoLock.lock();
        if (managedToSet == Boolean.FALSE) {
            //if we get here, it means that the lock already existed.
            //so, we subscribe to a channel, where we'll be notified when the result is available
            container.addMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message, byte[] pattern) {
                    result[0] = "message:" + new String(message.getBody());
                    pseudoLock.unlock();
                    container.removeMessageListener(this);
                }
            }, new ChannelTopic(notificationKey));

            //this is for the rare chance that the listener doesn't manage to subscribe to
            //the topic quickly enough (the value will still be there through)
            valueFromRedis = getValueFromRedis(resultKey);
            if (valueFromRedis != null) {
                return "before message: " + valueFromRedis;
            }

        } else {
            //we managed to get the lock. So, it's our job to do the calculation,
            //save the result to the cache, and notify listeners that the value is available
            result[0] = joinPoint.proceed();
            redisTemplate.opsForValue().set(resultKey, result[0].toString());
            redisTemplate.delete(lockKey);
            redisTemplate.convertAndSend(notificationKey, result[0]);
            pseudoLock.unlock();
        }

        //wait for the result to be available
        while (pseudoLock.isLocked()) {
            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        return result[0];
    }

    private Object getValueFromRedis(String key) {
        return redisTemplate.opsForValue().get(key);
    }

}
