package com.example.demo;

import com.example.demo.services.SlowServiceCaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.stream.IntStream;

@RestController
@Slf4j
public class TheController {
    @Autowired
    private SlowServiceCaller slowServiceCaller;
    @Autowired
    private ExecutorService executorService;

    @GetMapping
    public ResponseEntity<Void> callSlowService() {
        log.info("Start of request");
        IntStream.range(0, 100)
                .parallel()
                .mapToObj(x -> new Thread(() -> {
                            String result = slowServiceCaller.call();
                            log.info("Thread #{}, result {}", x, result);
                        })
                )
                .forEach(executorService::submit);

        return ResponseEntity.accepted().build();
    }
}
