package com.example.demo;

import java.util.concurrent.atomic.AtomicReference;

class PseudoLock {

    private final AtomicReference<Boolean> mylock = new AtomicReference<>(Boolean.FALSE);


    boolean lock() {
        return mylock.compareAndSet(Boolean.FALSE, Boolean.TRUE);
    }

    boolean isLocked() {
        return mylock.get() == Boolean.TRUE;
    }

    void unlock() {
        boolean done = mylock.compareAndSet(Boolean.TRUE, Boolean.FALSE);
        if (!done) {
            throw new IllegalStateException("Cannot unlock an unlocked thread");
        }
    }
}
