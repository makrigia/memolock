package com.example.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The result of the method is calculated based on the <a href="https://redislabs.com/blog/caches-promises-locks/">MemoLock</a> algorithm.
 *
 * @see MemoLockAspect for the MemoLock implenetation
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MemoLock {

}
